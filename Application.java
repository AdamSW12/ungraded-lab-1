public class Application {
	public static void main(String[] args) {
		GoblinShark john = new GoblinShark();
		GoblinShark dave = new GoblinShark();
		
		john.gender = "male";
		john.diet = "Nathan";
		john.name = "John";
		john.age = 32;
		
		dave.gender = "female";
		dave.diet = "Vlad";
		dave.name = "Dave";
		dave.age = 45;
		/*
		dave.sayName(dave.name);
		dave.playBall();
		dave.sayDiet(dave.diet);
		
		System.out.println();
		
		john.sayName(john.name);
		john.playBall();
		john.sayDiet(john.diet);
		*/
		
		GoblinShark[] shiver = new GoblinShark[3];
		shiver[0] = dave;
		shiver[1] = john;
		shiver[2] = new GoblinShark();
		
		shiver[2].gender = "female";
		shiver[2].diet = "Aral";
		shiver[2].name = "Safin";
		shiver[2].age = 12;
		
		shiver[0].sayName(shiver[0].name);
		shiver[1].sayName(shiver[1].name);
		shiver[2].sayName(shiver[2].name);
		
	}
}