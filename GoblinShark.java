public class GoblinShark {
	public String gender;
	public String diet;
	public String name;
	public int age;
	
	public void sayName(String name){
		System.out.println("My name is: " + name);
	}
	public void playBall() {
		System.out.println("I'm ballin' !");
	}
	public void sayDiet (String diet)
	{
		System.out.println("My diet consists of " + diet);
	}
}